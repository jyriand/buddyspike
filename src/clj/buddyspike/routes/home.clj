(ns buddyspike.routes.home
  (:require
    [buddyspike.layout :as layout]
    [buddyspike.db.core :as db]
    [clojure.java.io :as io]
    [buddyspike.middleware :as middleware]
    [ring.util.http-response :as response]
    [buddy.hashers :as hashers]))

(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn login-page [request]
  (layout/render request "about.html"))

(defn validate-user [username password]
  (if-let [user (db/get-user {:id username})]
    (let [pass (:pass user)]
      (when (hashers/check password pass)
        user))
    nil))

(defn handle-login [{:keys [params] :as request}]
  (let [username (:username params)
        password (:password params)
        user (validate-user username password)]
    (if user
      (layout/render-string request (str "Thank you for logging in " (:email user)))
      (layout/render-string request (str "Cant find user with username " username)))))

(defn signup-page [request]
  (layout/render request "signup.html"))

(defn create-user [username password]
  (let [pass-hash (hashers/derive password)
        user {:id username
              :pass pass-hash
              :first_name "J"
              :last_name "A"
              :email "jyriand@gmail.com"}]
    (db/create-user! user)))

(defn handle-signup [{:keys [params] :as request}]
  (let [username (:username params)
        password (:password params)]
    (if (create-user username password)
      (layout/render-string request (str "User created: " username))
      (layout/render-string request "User creation failed"))))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/signup" {:get signup-page
               :post handle-signup}]
   ["/login" {:get login-page
              :post handle-login}]])
