FROM openjdk:8-alpine

COPY target/uberjar/buddyspike.jar /buddyspike/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/buddyspike/app.jar"]
