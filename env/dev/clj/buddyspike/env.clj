(ns buddyspike.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [buddyspike.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[buddyspike started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[buddyspike has shut down successfully]=-"))
   :middleware wrap-dev})
